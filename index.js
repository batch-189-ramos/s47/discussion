// alert('Hello World!');

// Query Selector
// # - targets id of an/object field
// querySelector() is a method that can be used to select a specific element from our document
console.log(document.querySelector("#txt-first-name"));

// document refers to the whole page
console.log(document);


/* receives string

	Alternative, that we can use aside from the querySelector in retrieving elements

	document.getElemetById("txt-first-name");
	document.getElemetByClassName();
	document.getElemetByTagName();
*/

const txtFirstName = document.querySelector("#txt-first-name");

const spanFullName = document.querySelector("#span-full-name");

console.log(txtFirstName);
console.log(spanFullName);

/*
	EVENTS
		clicks, hover, keypress and many other things

	Event Listeners
		Allows us to lt our users interact with our page. Each click of hover is an event which can trigger a function/task

	Syntax:
		selectedElement.addEventListener('event', function);

*/

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
});


txtFirstName.addEventListener('keyup', (event) => {
	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
});





/*
	.innerHTML - is a property of an element which considers all the children of the selected element as a string.

	.value - input in the text field
	
*/



const labelFirstName = document.querySelector("#label-txt-name");
console.log(labelFirstName)

labelFirstName.addEventListener('click', (e) => {
	console.log(e)
	alert("You clicked first name label")
})
